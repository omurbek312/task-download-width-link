var button_download_images = $(".download-btn");
button_download_images.click(function () {
    var link = $("#download-img").val();
    var width = $("#img-width").val();
    var height = $("#img-height").val();
    if (link.trim().length > 5 && is_url(link.trim())) {
        $("section.wrapper.pop-up").css("display","flex");
        $("html").addClass("show-pop-up");
        $.post("/", {
            page_url: link,
            width: (width.length > 1) ? width : 30,
            height: (height.length > 1) ? height : 30
        }, function (response) {
            if (response.status == 200) {
                $("section.wrapper.pop-up").hide();
                if (response.images.length){
                $.each(response.images,function (k,item) {
                    var image = $(` <div class="image">
                             <img src="/images/${item}">
                         </div>`);
                    $("div.image-list").prepend(image)
                });
                }else{
                    alert("Don't found images width params!")
                }

                console.log(response);
            }
        }, "JSON").done(function () {
            $("html").removeClass("show-pop-up");
        })
    } else {
        alert("Wrong link");
    }
});

$('input.size').on('keydown', function (evt) {
    var key = evt.charCode || evt.keyCode || 0;

    return (key == 8 ||
        key == 9 ||
        key == 46 ||
        key == 110 ||
        key == 190 ||
        (key >= 35 && key <= 40) ||
        (key >= 48 && key <= 57) ||
        (key >= 96 && key <= 105));
});

function is_url(str)
{
    regexp =  /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
    if (regexp.test(str))
    {
        return true;
    }
    else
    {
        return false;
    }
}