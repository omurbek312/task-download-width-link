<?php
namespace App\Helper;
use \Gumlet\ImageResize;


class Core
{
  public static function getPage($url)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    curl_close($ch);
    return $return;
  }

  public static function uploadImage(string $url){
    try {
      $filename = basename($url);
       return (file_put_contents("images/" . $filename, file_get_contents($url)))? $filename : false;
    } catch (\Exception $e) {
    }
  }

  public static function getImageSize($url){
    $image_data = getimagesize($url);
    $image_size = explode(" ", $image_data[3]);
    $image_size = implode(",", $image_size);
    $size = [];
    $pattern_size = "#^width=\"(?<width>[0-9]+)\"\,height=\"(?<height>[0-9]+)\"$#";
    preg_match($pattern_size, $image_size, $size);
    return $size;
  }

  public static function getImagesInPage($url){
    $page = self::getPage($url);
    $pattern = '/<img(?:\\s[^<>]*?)?\\bsrc\\s*=\\s*(?|"([^"]*)"|\'([^\']*)\'|([^<>\'"\\s]*))[^<>]*>/i';
    preg_match_all($pattern, $page, $result);

    if (!file_exists("images")) {
      mkdir("images", 0700);
    }

    return $result[1] ?? [];
  }

  public static function getImageLink($image_link,$url_domain){
    $image_link = ltrim($image_link, "/");
    if (preg_match("#http://#", $image_link) || preg_match("#https://#", $image_link)) {
      $url = $image_link;
    } else {

      $url = $url_domain . $image_link;
    }
    return $url;
  }


  public static function setTextInImageJpg($image_name,$text){
    $img = "images/$image_name";
    $pic = ImageCreateFromjpeg($img);
    Header("Content-type: image/jpeg");
    $color=ImageColorAllocate($pic, 250, 0, 0);

    $h = 40; //height
    $w = 40; //width

    ImageTTFtext($pic, 18, 0, $w, $h, $color, "Font/ARIAL.TTF", $text);
    Imagejpeg($pic,"images/".$image_name);
    ImageDestroy($pic);
  }

  public static function setTextInImagePng($image_name,$text){
    $img = "images/$image_name";
    $pic = imagecreatefrompng($img);
    Header("Content-type: image/png");
    $color=ImageColorAllocate($pic, 250, 0, 0);

    $h = 40; //height
    $w = 40; //width

    ImageTTFtext($pic, 18, 0, $w, $h, $color, "Font/ARIAL.TTF", $text);
    imagepng($pic,"images/".$image_name);
    imagedestroy($pic);

  }

  public static function resizeImage($file_name){
    $image = new  ImageResize ("images/".$file_name);
    $image->resizeToLongSide ( 200 );
    $image->save("images/".$file_name);

    $image = new ImageResize ("images/".$file_name);
    $image->resizeToShortSide( 200 );
    $image->save("images/".$file_name);
  }
}

