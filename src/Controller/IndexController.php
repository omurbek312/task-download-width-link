<?php

namespace App\Controller;

use App\Entity\Images;
use App\Helper\Core as Helper;
use App\Helper\Core;
use App\Repository\ImagesRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Gumlet\ImageResize;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

  /**
   * @Route("/",name="index")
   * @Method("POST")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction(Request $request)
  {
    $params = $request->request->all();
    $status = 404;
    $images=[];
    $sizes_check = [];
    if ($params) {
      $url = $params["page_url"];
      $width = $params["width"];
      $height = $params["height"];
      if (isset($url) && $url) {
        $url_domain = [];
        $domain_pattern='#^(?<domain>(http://[a-z\.-]+|https://[a-z\.-]+))#';
        preg_match($domain_pattern,$url,$url_domain);

        $url_domain = $url_domain["domain"]."/";
        $result = Core::getImagesInPage($url);
        for ($i = 0; $i <= (count($result) - 1); $i++) {
          $image_link = $result[$i];
          $image_link = Core::getImageLink($image_link, $url_domain);
          try {
            $size = Helper::getImageSize($image_link);

            if ($size) {
              if ((int)$size['width'] >= (int)$width && (int)$size['height'] >= (int)$height) {
                $file_name = Helper::uploadImage($image_link);
                if ($status)
                  $status = ImagesRepository::saveImage($file_name, $this->getDoctrine()->getManager());
                $images[]=$file_name;
                $sizes_check[]=["width"=>$size['width'],"height"=>$size['height']];
                Core::resizeImage($file_name);
                if (explode(".", $file_name)[1] == "png") {
                  Core::setTextInImagePng($file_name, "test text?");
                } elseif (explode(".", $file_name)[1] == "jpg") {
                  Core::setTextInImageJpg($file_name, "test text?");
                }
              }
            }
          }catch (\Exception $e){
            print_r("error $e");
            die();
          }
        }
        return exit(json_encode([
          "status" => ($status) ? 200 : 404,
          "images"=>$images,
          "sizes"=>$sizes_check
        ]));
      }
    }
    $repository = $this->getDoctrine()->getRepository(Images::class);
    $images = $repository->findAll();
    return $this->render("index/index.html.twig",["images"=>$images??[]]);

  }

  /**
   * @Route("/images")
   * @Method("POST")
   * @param ObjectManager $em
   */
  public function getImagesAction(ObjectManager $em)
  {
    $images = $em->getRepository(Images::class)->findAll();
    return exit(json_encode(["images"=>$images ?? false]));
  }

  /**
   * @Route("/test")
   * @Method("GET")
   * @param Request $request
   */
  public function testAction(Request $request)
  {   $text = $request->query->get("text");
      print_r($text);
      Core::setTextInImageJpg("led_pro.jpg",$text);
      Core::setTextInImagePng("nakal.png",$text);
      die();
  }


}