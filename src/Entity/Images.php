<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagesRepository")
 * @ORM\Table(name="image")
 */


class Images
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;
  /**
   * @var string $file
   * @ORM\Column(type="string")
   *
   */
  private $file;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }
  /**
   * @param mixed $id
   */
  public function setId($id): void
  {
    $this->id = $id;
  }
  /**
   * @return string
   */
  public function getFile()
  {
    return $this->file;
  }
  /**
   * @param string $file
   */
  public function setFile($file)
  {
    if ($file) {
      $this->file = $file;
    }
  }

}